from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.core.cache import cache
import pika
import json
import hashlib
# Create your views here.
def index(request):
    if request.method == "GET":
        return render(request, 'testuas/index.html')
    else :
        username = request.POST['username']
        password = request.POST['password']
        myfile = request.FILES['userfile']
        fs = FileSystemStorage()
        filename = fs.save(myfile.name, myfile)
        ext = filename.split(".")[-1]
        filename_ = hashlib.sha256(bytes(filename, "utf-8")).hexdigest()
        cache.set(filename_,open(filename,"rb").read())
        cred = pika.PlainCredentials('1406559036','1406559036')
        connection = pika.BlockingConnection(pika.ConnectionParameters(host='152.118.148.103',port=5672,virtual_host='/1406559036',credentials=cred,heartbeat=10))
        channel = connection.channel()
        mesg = json.dumps({"username":username,"password":password,"filename":filename_})

        channel.exchange_declare(exchange='ORCHES',exchange_type='fanout')
        channel.basic_publish(exchange='ORCHES',routing_key='',body=mesg)
        return render(request, 'testuas/index.html')
