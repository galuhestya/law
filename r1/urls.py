from django.urls import path
from . import views

urlpatterns = [
    path("", views.compute, name="compute"),
    path("a1", views.a1, name="a1")
]
