from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('auth/login',views.auth, name="login"),
    path('comments',views.comment, name="comments"),
    path('comments/<int:ids>', views.get_comment, name="get_comment"),
    path('users', views.users, name="users"),
    path('validate',views.validate, name="validate"),
]