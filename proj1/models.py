from django.db import models
import datetime

# Create your models here.
class User(models.Model):
	username = models.CharField(max_length=255)
	display_name = models.CharField(max_length=255, blank=True)

class Comment(models.Model):
	comment = models.TextField()
	created_by = models.CharField(max_length=255)
	created_at = models.DateTimeField(default=datetime.datetime.now().isoformat())
	updated_at = models.DateTimeField(default=datetime.datetime.now().isoformat())