from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from django.core.cache import cache
from dateutil import parser
import requests
import socket
import json
import datetime
from django.views.decorators.csrf import csrf_exempt
from proj1.models import User, Comment

CLIENT_ID = "jwLbiel1DD6pZvNm86rYemUZVTirgvw1"

@csrf_exempt
def index(request):
	return JsonResponse({})

def get_token(username,password):
	token_url = "http://172.22.0.2/oauth/token"
	header = {
		"Content-Type" : "application/x-www-form-urlencoded",
		"Accept" : "application/json",
	}
	param = {
		"username" : username,
		"password" : password,
		"client_id" : CLIENT_ID,
		"client_secret" : "TyXPByQmB4EOKMOyG5iD3VDOEUzEFset",
		"grant_type" : "password",
	}

	token = requests.post(token_url,headers=header,data=param).json()
	try:
		token = token["access_token"]
		return token
	except:
		return None

def validate_token(token):
	validation_url = "http://172.22.0.2/oauth/resource"
	header = {
		"Authorization" : "Bearer %s" %(token),
	}
	try :
		r = requests.get(validation_url,headers=header).json()
		if(r["client_id"] == CLIENT_ID):
			return r["user_id"]
		else:
			return None
	except:
		return None

@csrf_exempt
def validate(request):
	token = request.POST["token"]
	validation_url = "http://172.22.0.2/oauth/resource"
	header = {
		"Authorization" : "Bearer %s" %(token),
	}
	try :
		r = requests.get(validation_url,headers=header).json()
		if(r["client_id"] == CLIENT_ID):
			response = {"status":"ok"}
			return JsonResponse(response)
		else:
			return JsonResponse({"status":"invalid"})
	except:
		return JsonResponse({"status":"error"})


@csrf_exempt
def auth(request):

	# body_unicode = request.body.decode('utf-8')
	# body_data = json.loads(body_unicode)
	body_data = request.POST
	username = body_data['username']
	password = body_data['password']

	token = get_token(username, password)
	if(token == None):
		return error_response(401,"Unauthorized")
	request.session['token'] = token

	response = {
		"status" : "ok",
		"token" : token,
	}

	return JsonResponse(response)

@csrf_exempt
def comment(request):
	bearer = request.META.get("HTTP_AUTHORIZATION")
	try:
		token = bearer.split(" ")
		username = validate_token(token[1])
	except IndexError:
		return error_response(401,"Bad Request")
	except AttributeError:
		return error_response(401,"Bad Request")

	if request.method == "GET":
		page = request.GET.get("page")
		limit = request.GET.get("limit")
		createdBy = request.GET.get("createdBy")
		startDate = request.GET.get("startDate")
		endDate = request.GET.get("endDate")

		if(page == None or limit == None or createdBy == None or startDate == None or endDate == None):
			return error_response(400,"Bad Request. Parameter is not complete")

		c = Comment.objects.filter(created_at__range=(parser.parse(startDate),parser.parse(endDate)),created_by__iexact=createdBy).all()
		count = c.count()
		start = (int(page)-1)*int(limit)
		c = c[start:start+int(limit)]

		response = {
			"status" : "ok",
			"page" : page,
			"limit" : limit,
			"total" : count,
			"data" : list(c.values()),
		}
	elif request.method == "POST":
		if username == None:
			return error_response(401,"Unauthorized")

		body_unicode = request.body.decode('utf-8')
		body_data = json.loads(body_unicode)

		comment = body_data['comment']
		if(comment == None):
			return error_response(400,"Bad Request. Incomplete Request")

		displayName = User.objects.filter(username__iexact=username).first().display_name

		c = Comment(comment=comment,created_by=displayName)
		c.save()
		response = {
			"status" : "ok",
			"data" : {
				"id" : c.id,
				"createdBy" : c.created_by,
				"createdAt" : c.created_at,
				"updatedAt" : c.updated_at,
			},
		}
	elif request.method == "PUT":
		if username == None:
			return error_response(401,"Unauthorized")

		try:
			body_unicode = request.body.decode('utf-8')
			body_data = json.loads(body_unicode)

			ids = body_data['id']
			comment = body_data['comment']
		except IndexError:
			return error_response(400,"Bad Request. Incomplete Request")
		except ValueError:
			return error_response(400,"Bad Request. Invalid JSON")

		if(ids == None or comment == None):
			return error_response(400, "Bad Request. Incomplete Request")

		displayName = User.objects.filter(username__iexact=username).first().display_name

		try:
			c = Comment.objects.get(id=ids)
		except Comment.DoesNotExist:
			return error_response(400,"Bad Request. Comment id does not exists")

		if(c.created_by != displayName):
			return error_response(403,"Forbidden")
		c.comment = comment
		c.updated_at = datetime.datetime.now().isoformat()
		c.save()

		response = {
			"status" : "ok",
			"data" : {
				"id" : c.id,
				"comment" : comment,
				"createdBy" : displayName,
				"createdAt" : c.created_at,
				"updatedAt" : c.updated_at,
			},
		}
	elif request.method == "DELETE":
		if username == None:
			return error_response(401,"Unauthorized")

		displayName = User.objects.filter(username__iexact=username).first().display_name


		try:
			body_unicode = request.body.decode('utf-8')
			body_data = json.loads(body_unicode)

			ids = body_data['id']
		except IndexError:
			return error_response(400,"Bad Request. Incomplete Request")
		except ValueError:
			return error_response(400,"Bad Request. Invalid JSON")

		if(ids == None):
			return error_response(400,"Bad Request. Incomplete Request")

		c = Comment.objects.get(id=ids)

		if(c.created_by != displayName):
			return error_response(403,"Forbidden")

		c.delete()
		response = {
			"status" : "ok",
		}

	return JsonResponse(response)

def get_comment(request,ids):
	c = Comment.objects.get(id__exact=ids)
	response = {
		"status" : "ok",
		"data" : {
			"id" : c.id,
			"comment" : c.comment,
			"createdBy" : c.created_by,
			"createdAt" : c.created_at,
			"updatedAt" : c.updated_at,
		}
	}
	return JsonResponse(response)

@csrf_exempt
def users(request):
	bearer = request.META.get("HTTP_AUTHORIZATION")
	try:
		token = bearer.split(" ")
		username = validate_token(token[1])
	except IndexError:
		return error_response(401,"Bad Request")
	except AttributeError:
		return error_response(401,"Bad Request")

	cache.expire(token[1],timeout=1200)

	if request.method == "GET":
		page = request.GET.get("page")
		limit = request.GET.get("limit")

		if(page == None or limit == None):
			return error_response(400,"Bad Request. Incomplete Request")

		u = User.objects.all()
		count = u.count()
		start = (int(page)-1)*int(limit)
		u = u[start:start+int(limit)]

		response = {
			"status" : "ok",
			"page" : page,
			"limit" : limit,
			"total" : count,
			"data" : list(u.values()),
		}
	elif request.method == "POST":
		if(username == None):
			return error_response(401,"Unauthorized. Unknown Token")

		body_unicode = request.body.decode('utf-8')
		body_data = json.loads(body_unicode)

		displayName = body_data["displayName"]

		if(displayName == None):
			return error_response(400, "Bad Request. Incomplete Request")

		user = User(username=username,display_name=displayName)
		user.save()

		response = {
			"status" : "ok",
			"userId" : user.id,
			"displayName" : displayName,
		}

	return JsonResponse(response)

def error_response(code,description):
	return JsonResponse({
		"status" : "error",
		"description" : "%d %s" % (code, description),
	})
