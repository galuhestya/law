from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, JsonResponse
import logging
import json

logger = logging.getLogger(__name__)


@csrf_exempt
def addition(request):

	if request.method != "POST":
		response = {
			'status' : "error",
			"message" : "UNKNOWN METHOD",
		}
		logger.error('wrong method')
		return JsonResponse(response)

	body = request.body

	body_unicode = body.decode('utf-8')
	body_data = json.loads(body_unicode)

	a = body_data["a"]
	b = body_data["b"]

	logger.info('"A" : %s; "B" : %s' %(a,b))
	try :
		a = int(a)
		b = int(b)
	except ValueError:
		response = {
			"status" : "error",
			"message" : "ValueError"
		}
		logger.error('VALUE ERROR "A" : %s; "B" : %s' %(a,b))
		return JsonResponse(response)

	result = a+b
	response = {
		"status" : "success",
		"result" : result,
	}

	return JsonResponse(response)
