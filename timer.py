import os
import types
import zipfile
from functools import partial
import pika
import json
import datetime
import time
import sys

cred = pika.PlainCredentials('1406559036','1406559036')
connection = pika.BlockingConnection(pika.ConnectionParameters(host='152.118.148.103',port=5672,virtual_host='/1406559036',credentials=cred,heartbeat=10))

channel = connection.channel()

channel.exchange_declare(exchange='timer',exchange_type='fanout')

while True:
    mesg = json.dumps({'ts':str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))})
    channel.basic_publish(exchange='timer',routing_key='',body=mesg)
    time.sleep(1)
    
connection.close()
