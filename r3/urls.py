from django.urls import path
from . import views

urlpatterns = [
    path("", views.compute, name="compute"),
    path("a3", views.a3, name="a3"),
]
