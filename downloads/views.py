from django.shortcuts import render
from django.http import HttpResponse
from django.core.cache import cache
# Create your views here.
def index(request,filename):
	md5 = request.GET['md5']
	# filepath = "./"+filename+".bzip"
	# with open(filepath, 'rb') as fp:
	# 	data = fp.read()
	data = cache.get(filename)
	filename = filename+".bzip"
	response = HttpResponse(content_type="application/force-download")
	response['Content-Disposition'] = 'attachment; filename=%s' % filename # force browser to download file
	response.write(data)
	return response