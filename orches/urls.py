from django.urls import path
from . import views

urlpatterns = [
    path("", views.index, name="index"),
    path("compute",views.compute, name="compute"),
    path("tugas",views.tugas, name="tugas"),
    path("total",views.get_total, name="total"),
    path("plus", views.plus, name="plus"),
    path("minus", views.minus, name="minus"),
    path("multiple", views.multiple, name="multiple"),
    path("divide", views.divide, name="divide"),
    path("modulo", views.modulo, name="modulo"),
    path("wsdl", views.wsdl_trial, name="trial"),
]
