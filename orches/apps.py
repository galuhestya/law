from django.apps import AppConfig


class OrchesConfig(AppConfig):
    name = 'orches'
