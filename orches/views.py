from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
import requests
import json
from django.views.decorators.csrf import csrf_exempt
from django.core.cache import cache
import zeep
from multiprocessing.dummy import Pool as ThreadPool 
import itertools

urls = [
        "http://host20012.proxy.infralabs.cs.ui.ac.id/r1/a1",
        "http://host20012.proxy.infralabs.cs.ui.ac.id/r2/a2",
        "http://host20012.proxy.infralabs.cs.ui.ac.id/r3/a3"
        ]

def index(request):
    return render(request, 'orches/index.html')

def tugas(request):
    return render(request, 'orches/tugas.html')

# Create your views here.
@csrf_exempt
def compute(request):
    a = request.GET['a']
    b = request.GET['b']
    c = request.GET['c']
    d = request.GET['d']

    total = kurang(tambah(A(a,b,c,d),B(a,b,c,d)),C(a,b,c,d))
    response = {
        "status" : "OK",
        "total" : total,
    }
    return JsonResponse(response)

@csrf_exempt
def get_total(request):
    a = request.GET['a']
    b = request.GET['b']
    c = request.GET['c']
    d = request.GET['d']

    pool = ThreadPool(4) 
    res = pool.starmap(get_part, zip([1,2,3], urls,itertools.repeat(a), itertools.repeat(b),itertools.repeat(c), itertools.repeat(d)))
    pool.close()
    pool.join()
    # a1 = get_part(url_a,a,b,c,d)
    # a2 = get_part(url_b,a,b,c,d)
    # a3 = get_part(url_c,a,b,c,d)
    for i in res:
        if(i["from"] == 1):
            a1 = i["hasil"]
        elif(i["from"] == 2):
            a2 = i["hasil"]
        elif(i["from"] == 3):
            a3 = i["hasil"]
    total = kurang(tambah(a1,a2),a3)
    response = {
        "status" : "OK",
        "total" : total,
    }

    return JsonResponse(response)
    
def get_part(num, url,a,b,c,d):
    params = {
        "a" : a,
        "b" : b,
        "c" : c,
        "d" : d,
    }
    hasil = requests.get(url,params=params).json()
    response = {
        "status" : "OK",
        "from" : num,
        "hasil" : hasil["hasil"],
    }
    return response

@csrf_exempt
def wsdl_trial(request):
    wsdl = 'http://www.soapclient.com/xml/soapresponder.wsdl'
    client = zeep.Client(wsdl=wsdl)
    val = client.service.Method1('Zeep', 'is cool')
    response = {
        "status" : "OK",
        "response" : val
    }
    return JsonResponse(response)

def A(a,b,c,d):
    params = {
        "a" : a,
        "b" : b,
        "c" : c,
        "d" : d,
    }
    hasil = requests.get("http://host22012.proxy.infralabs.cs.ui.ac.id/r1",params=params).json()

    return hasil["hasil"]

def B(a,b,c,d):
    params = {
        "a" : a,
        "b" : b,
        "c" : c,
        "d" : d,
    }
    hasil = requests.get("http://host23012.proxy.infralabs.cs.ui.ac.id/r1",params=params).json()
    return hasil["hasil"]

def C(a,b,c,d): 
    params = {
        "a" : a,
        "b" : b,
        "c" : c,
        "d" : d,
    }
    hasil = requests.get("http://host24012.proxy.infralabs.cs.ui.ac.id/r1",params=params).json()

    return hasil["hasil"]

def tambah(a,b):
    key = str(a)+"+"+str(b)
    if(not cache.get(key) == None):
        return cache.get(key)

    param = {
     'a' : a,
     'b' : b,
    }
    total = requests.get("http://host20099.proxy.infralabs.cs.ui.ac.id/tambah.php",params=param).json()
    hasil = total["hasil"]
    cache.set(key, hasil)
    return hasil

def kurang(a,b):
    key = str(a)+"-"+str(b)
    if(not cache.get(key) == None):
        return cache.get(key)

    param = {
     'a' : a,
     'b' : b,
    }
    total = requests.post("http://host20099.proxy.infralabs.cs.ui.ac.id/kurang.php",data=param)

    cache.set(key, total.text)
    return total.text

def kali(a,b):
    key = str(a)+"*"+str(b)
    if(not cache.get(key) == None):
        return cache.get(key)

    param = {
        "Argumen-A" : a,
        "Argumen-B" : b,
    }

    total = requests.get("http://host20099.proxy.infralabs.cs.ui.ac.id/kali.php",headers=param)
    cache.set(key, total.text)

    return total.text

def bagi(a,b):
    key = str(a)+"/"+str(b)
    if(not cache.get(key) == None):
        return cache.get(key)

    param = {
        "Argumen-A" : a,
        "Argumen-B" : b,
    }
    total = requests.head("http://host20099.proxy.infralabs.cs.ui.ac.id/bagi.php",headers=param)

    cache.set(key, total.headers.get("hasil"))
    return total.headers.get("hasil")

def modulo_(a,b):
    key = str(a)+"%"+str(b)
    if(not cache.get(key) == None):
        return cache.get(key)

    wsdl = 'http://host20099.proxy.infralabs.cs.ui.ac.id/matematika.xml'
    client = zeep.Client(wsdl=wsdl)
    val = client.service.modulo(a, b)
    cache.set(key, val)
    return val

def round_(a):
    key = "r"+str(a)
    if(not cache.get(key) == None):
        return cache.get(key)

    wsdl = 'http://host20099.proxy.infralabs.cs.ui.ac.id/matematika.xml'
    client = zeep.Client(wsdl=wsdl)
    val = client.service.round_number(a)
    cache.set(key, val)
    return val

@csrf_exempt
def modulo(request):
    a = request.GET['a']
    b = request.GET['b']

    wsdl = 'http://host20099.proxy.infralabs.cs.ui.ac.id/matematika.xml'
    client = zeep.Client(wsdl=wsdl)
    val = client.service.modulo(a, b)

    response = {
        "status" : "OK",
        "total" : val,
    }
    return JsonResponse(response)

@csrf_exempt
def round(request):
    a = request.GET['a']

    wsdl = 'http://host20099.proxy.infralabs.cs.ui.ac.id/matematika.xml'
    client = zeep.Client(wsdl=wsdl)
    val = client.service.round_number(a)

    response = {
        "status" : "OK",
        "total" : val,
    }
    return JsonResponse(response)

@csrf_exempt
def plus(request):
    a = request.GET['a']
    b = request.GET['b']
    param = {
     'a' : a,
     'b' : b,
    }
    total = requests.get("http://host20099.proxy.infralabs.cs.ui.ac.id/tambah.php",params=param).json()
    hasil = total["hasil"]
    response = {
        "status" : "OK",
        "total" : hasil,
    }
    return JsonResponse(response)

@csrf_exempt
def minus(request):
    a = request.GET['a']
    b = request.GET['b']

    param = {
     'a' : a,
     'b' : b,
    }
    total = requests.post("http://host20099.proxy.infralabs.cs.ui.ac.id/kurang.php",data=param)
    response = {
        "status" : "OK",
        "total" : total.text,
    }
    return JsonResponse(response)

@csrf_exempt
def multiple(request):
    a = request.GET['a']
    b = request.GET['b']

    param = {
        "Argumen-A" : a,
        "Argumen-B" : b,
    }

    total = requests.get("http://host20099.proxy.infralabs.cs.ui.ac.id/kali.php",headers=param)

    response = {
        "status" : "OK",
        "total" : total.text,
    }
    return JsonResponse(response)

@csrf_exempt
def divide(request):
    a = request.GET['a']
    b = request.GET['b']

    param = {
        "Argumen-A" : a,
        "Argumen-B" : b,
    }

    total = requests.head("http://host20099.proxy.infralabs.cs.ui.ac.id/bagi.php",headers=param)
    response = {
        "status" : "OK",
        "total" : total.headers.get("hasil"),
    }
    return JsonResponse(response)
