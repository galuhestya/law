"""laws URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import include

urlpatterns = [
   path('api/v1/', include('proj1.urls')),
   path('oauth/',include('oauth.urls')),
   path('thumbnail/',include('thumbnails.urls')),
   path('add', include('addition.urls')),
   path('uploads',include('uploads.urls')),
   path('orches/', include('orches.urls')),
   path('r1/',include('r1.urls')),
   path('r2/',include('r2.urls')),
   path('r3/',include('r3.urls')),
   path('uas1/',include('testuas.urls')),
   path('compress/',include('ucompressor.urls')),
   path('encrypt/',include('encryptor.urls')),
   path('downloads/',include('downloads.urls')),
]
