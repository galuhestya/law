from django.urls import path
from . import views

urlpatterns = [
    path("",views.index, name="index"),
    path("service",views.thumbnail_service, name="service"),
    path("nginx",views.thumbnail_nginx, name="nginx"),
]
