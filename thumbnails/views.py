from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from PIL import Image
# Create your views here.
def index(request):
	image = open("thumbnails/media/img/SSMountain500.jpg",'rb').read()
	return HttpResponse(image, content_type="image/jpeg")

def thumbnail_service(request):
	image_path = "thumbnails/media/img/"
	thumbnail_path = "thumbnails/media/thumbnail/"

	SIZE = 100,100
	im = Image.open(image_path+"SSMountain500.jpg")
	im.thumbnail(SIZE, Image.ANTIALIAS)
	im.save(thumbnail_path+"SSMountain500.jpg", 'JPEG', quality=80)
	image = open(thumbnail_path+"SSMountain500.jpg",'rb').read()
	return HttpResponse(image, content_type="image/jpeg")

def thumbnail_nginx(request):
	image = open("thumbnails/media/img/SSMountain500.jpg",'rb').read()
	return HttpResponse(image, content_type="image/jpeg")
