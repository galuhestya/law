from django.urls import path
from . import views

urlpatterns = [
    path("", views.compute, name="compute"),
    path("a2", views.a2, name="a2"),
]
