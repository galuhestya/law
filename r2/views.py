from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
import requests
import json
from django.views.decorators.csrf import csrf_exempt
from django.core.cache import cache
import zeep

def compute(request):
	a = request.GET['a']
	b = request.GET['b']
	c = request.GET['c']
	d = request.GET['d']
	total = bagi(tambah(a,d),modulo_(b,c))
	response = {
	"status" : "OK",
	"hasil" : total,
	}
	return JsonResponse(response)

def a2(request):
    a = request.GET['a']
    b = request.GET['b']
    c = request.GET['c']
    d = request.GET['d']
    total = bagi(kali(tambah(tambah(a,b),c),d),c)
    response = {
    "status" : "OK",
    "hasil" : total,
    }
    return JsonResponse(response)

def tambah(a,b):
    key = str(a)+"+"+str(b)
    if(not cache.get(key) == None):
        return cache.get(key)

    param = {
     'a' : a,
     'b' : b,
    }
    total = requests.get("http://host20099.proxy.infralabs.cs.ui.ac.id/tambah.php",params=param).json()
    hasil = total["hasil"]
    cache.set(key, hasil)
    return hasil

def kurang(a,b):
    key = str(a)+"-"+str(b)
    if(not cache.get(key) == None):
        return cache.get(key)

    param = {
     'a' : a,
     'b' : b,
    }
    total = requests.post("http://host20099.proxy.infralabs.cs.ui.ac.id/kurang.php",data=param)

    cache.set(key, total.text)
    return total.text

def kali(a,b):
    key = str(a)+"*"+str(b)
    if(not cache.get(key) == None):
        return cache.get(key)

    param = {
        "Argumen-A" : a,
        "Argumen-B" : b,
    }

    total = requests.get("http://host20099.proxy.infralabs.cs.ui.ac.id/kali.php",headers=param)
    cache.set(key, total.text)

    return total.text

def bagi(a,b):
    key = str(a)+"/"+str(b)
    if(not cache.get(key) == None):
        return cache.get(key)

    param = {
        "Argumen-A" : a,
        "Argumen-B" : b,
    }
    total = requests.head("http://host20099.proxy.infralabs.cs.ui.ac.id/bagi.php",headers=param)

    cache.set(key, total.headers.get("hasil"))
    return total.headers.get("hasil")

def modulo_(a,b):
    key = str(a)+"%"+str(b)
    if(not cache.get(key) == None):
        return cache.get(key)

    wsdl = 'http://host20099.proxy.infralabs.cs.ui.ac.id/matematika.xml'
    client = zeep.Client(wsdl=wsdl)
    val = client.service.modulo(a, b)
    cache.set(key, val)
    return val

def round_(a):
    key = "r"+str(a)
    if(not cache.get(key) == None):
        return cache.get(key)

    wsdl = 'http://host20099.proxy.infralabs.cs.ui.ac.id/matematika.xml'
    client = zeep.Client(wsdl=wsdl)
    val = client.service.round_number(a)
    cache.set(key, val)
    return val