from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.conf import settings
from django.core.files.storage import FileSystemStorage
# Create your views here.
def index(request):
    if request.method == "GET":
        return render(request, 'uploads/index.html')
    else :
        myfile = request.FILES['userfile']
        fs = FileSystemStorage()
        filename = fs.save(myfile.name, myfile)
        uploaded_file_url = fs.url(filename)
        return JsonResponse({"status":"OK","location":uploaded_file_url})
