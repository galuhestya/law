import os
import types
import zipfile
from functools import partial
import pika
import json
import datetime
import time
import sys
import redis

def progress(total_size, original_write, self, buf):
    progress.bytes += len(buf)
    progress.obytes += 1024 * 8  # Hardcoded in zipfile.write
    progressx = int(100 * progress.obytes / total_size)
    print("{} % done".format(int(100 * progress.obytes / total_size)))
    mesg = json.dumps({'action':'compress','status':'0','progress':progressx})
    channels.basic_publish(exchange='compress',routing_key='',body=mesg)

    return original_write(buf)

    # print("{} bytes written".format(progress.bytes))
    # print("{} original bytes handled".format(progress.obytes))


def compress(mesg):
    in_file = mesg['filename']
    token = mesg['token']
    out_file = in_file+".bzip"
    progress.bytes = 0
    progress.obytes = 0

    with zipfile.ZipFile(out_file, 'w', compression=zipfile.ZIP_DEFLATED) as _zip:
        # Replace original write() with a wrapper to track progress
        _zip.fp.write = types.MethodType(partial(progress, os.path.getsize(in_file),
                                                 _zip.fp.write), _zip.fp)
        _zip.write(in_file)


    mesg = json.dumps({'action':'encrypt','status':'1','filename':in_file,'token':token})
    channels.basic_publish(exchange='ENCRYPT',routing_key='',body=mesg)
    
    return out_file

cred = pika.PlainCredentials('1406559036','1406559036')
connection = pika.BlockingConnection(pika.ConnectionParameters(host='152.118.148.103',port=5672,virtual_host='/1406559036',credentials=cred,heartbeat=10))

channels = connection.channel()
channels.exchange_declare(exchange='compress',durable=True,exchange_type='fanout')

result = channels.queue_declare(durable=True,exclusive=True)
queue_name = result.method.queue
channels.queue_bind(exchange='compress',routing_key='',queue=queue_name)

channel = connection.channel()

channel.exchange_declare(exchange='COMPRESS_SEND',exchange_type='fanout')

result = channel.queue_declare(exclusive=True)
queue_name = result.method.queue
channel.queue_bind(exchange='COMPRESS_SEND',queue=queue_name)

r = redis.Redis(
    host='172.22.0.61',
    port=6379, 
    password='')
def callback(ch, method, properties, body):
        try:
            strs = body.decode("utf-8")
            j = json.loads(strs)
        except:
                return "JSON ERROR"
        time.sleep(10)
        compressed = compress(j)
        a = open(compressed,"rb")
        r.set(j['filename'],a.read())



channel.basic_consume(callback,
                      queue=queue_name,
                      no_ack=True)
channel.start_consuming()
