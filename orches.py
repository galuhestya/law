import os
import types
import zipfile
from functools import partial
import pika
import json
import datetime
import time
import sys
import requests

def do_stuff(mesg):
    username = mesg["username"]
    password = mesg["password"]
    filename = mesg["filename"]
    payload = {'username':username, 'password':password}
    response = requests.post("http://host21012.proxy.infralabs.cs.ui.ac.id/api/v1/auth/login",data=payload).json()
    # print(response)
    try:
        token = response["token"]
    except:
        print("WRONG credentials")
    payload = {'token':token,'filename':filename}
    s = requests.post("http://host23012.proxy.infralabs.cs.ui.ac.id/compress/",data=payload).json()
    if(s["status"] != "ok"):
        return "ERROR"
    resp = {'status':'ok'}
    return resp

def do_stuff2(mesg):
    token = mesg['token']
    filename = mesg['filename']
    payload = {"token":token,"filename":filename}
    s = requests.post("http://host24012.proxy.infralabs.cs.ui.ac.id/encrypt/",data=payload).json()
    if(s["status"] != "1"):
        return "ERROR"
    url = s["url"]
    channels = connection.channel()
    channels.exchange_declare(exchange='compress',durable=True,exchange_type='fanout')

    result = channels.queue_declare(durable=True,exclusive=True)
    queue_name = result.method.queue
    channels.queue_bind(exchange='compress',routing_key='',queue=queue_name)

    mesg = json.dumps({'action':'done','status':'2',"url":url})
    channels.basic_publish(exchange='compress',routing_key='',body=mesg)
    resp = {'status':'ok'}
    return resp

cred = pika.PlainCredentials('1406559036','1406559036')
connection = pika.BlockingConnection(pika.ConnectionParameters(host='152.118.148.103',port=5672,virtual_host='/1406559036',credentials=cred,heartbeat=10))

channel = connection.channel()

channel.exchange_declare(exchange='ORCHES',exchange_type='fanout')

result = channel.queue_declare(exclusive=True)
queue_name = result.method.queue
channel.queue_bind(exchange='ORCHES',queue=queue_name)

def callback(ch, method, properties, body):
        try:
            strs = body.decode("utf-8")
            j = json.loads(strs)
        except:
                return "JSON ERROR"
        do_stuff(j)

channel.basic_consume(callback,
                      queue=queue_name,
                      no_ack=True)

channel.exchange_declare(exchange='ENCRYPT',exchange_type='fanout')
result = channel.queue_declare(exclusive=True)
queue_name = result.method.queue
channel.queue_bind(exchange='ENCRYPT',queue=queue_name)

def callback2(ch, method, properties, body):
        try:
            strs = body.decode("utf-8")
            j = json.loads(strs)
        except:
                return "JSON ERROR"
        do_stuff2(j)

channel.basic_consume(callback2,
                      queue=queue_name,
                      no_ack=True)

channel.start_consuming()
