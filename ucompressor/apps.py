from django.apps import AppConfig


class UcompressorConfig(AppConfig):
    name = 'ucompressor'
