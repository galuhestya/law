from django.shortcuts import render
import requests
import pika
from django.core.cache import cache
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
import json
@csrf_exempt
def index(request):
	token = request.POST["token"]
	filename = request.POST["filename"]
	payload = {'token':token}
	response = requests.post("http://host21012.proxy.infralabs.cs.ui.ac.id/api/v1/validate",data=payload).json()
	
	if(response["status"] != "ok"):
		return "ERROR"
	#write byte to file in server 3
	_file = cache.get(filename)
	f = open(filename,"wb")
	f.write(_file)
	f.close()
	#start sending message to compressor
	cred = pika.PlainCredentials('1406559036','1406559036')
	connection = pika.BlockingConnection(pika.ConnectionParameters(host='152.118.148.103',port=5672,virtual_host='/1406559036',credentials=cred,heartbeat=10))
	channel = connection.channel()
	mesg = json.dumps({"filename":filename,"token":token})

	channel.exchange_declare(exchange='COMPRESS_SEND',exchange_type='fanout')
	channel.basic_publish(exchange='COMPRESS_SEND',routing_key='',body=mesg)
	return JsonResponse({'status':'ok'})