from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.core.cache import cache
import requests
import socket
import json

server_key = "de4b2f7dd51256c9d737bbd26558fd40fa66891ecddd3d16c68bb048dd7fbdca"
server_secret = "6694fd89068a1afd241385a54401f9ec0b9778445cea799eb7bc12774f22fbee"
server_callback = "http://host20012.proxy.infralabs.cs.ui.ac.id/git/callback"

local_key = "fc43120adc532a7e14a1bd6d85e20d883287e7a5014463120ab05d547bf7c827"
local_secret = "fc1417d64f652d785fed73d1c02406b4dd6ca5b3a5f0a7297c460b49bb41847f"
local_callback = "http://localhost:8000/git/callback"

def index(request):
	ip = socket.gethostbyname(socket.gethostname())
	context = {'ip' : ip}
	return HttpResponse(render(request, 'github/index.html', context))

def auth(request):
	return redirect('https://gitlab.com/oauth/authorize?client_id='+server_key+'&redirect_uri='+server_callback+'&response_type=code')

def callback(request):
	key = request.GET['code']
	data_auth = {
		"client_id" : server_key,
		"client_secret" : server_secret,
		"grant_type" : "authorization_code",
		"redirect_uri" : server_callback,
		"code" : key,
	}
	token = requests.post('https://gitlab.com/oauth/token', data=data_auth).json()
	rs = requests.get("https://gitlab.com/api/v4/user?access_token="+token['access_token']).json()
	cache.set(rs['username'],token['access_token'])
	
	return redirect("/git/user/"+rs['username'])

def user(request, key):
	token = cache.get(key)
	if(token == None):
		return redirect('/git')
	rs = requests.get("https://gitlab.com/api/v4/user?access_token="+token)
	if(rs.status_code != requests.codes.ok):
		return redirect('/git')
	rs = rs.json()
	ip = socket.gethostbyname(socket.gethostname())
	context = {
		"ip" : ip,
		"json" : rs,
	}

	return HttpResponse(render(request, 'github/user.html', context))