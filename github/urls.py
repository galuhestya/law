from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('auth/gitlab',views.auth, name='auth'),
    path('callback', views.callback, name='callback'),
    path('user/<str:key>', views.user, name='user'),
]