from django.shortcuts import render
import requests
import pika
from django.core.cache import cache
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
import json
import hashlib
import time
import base64

@csrf_exempt
def index(request):
	token = request.POST["token"]
	filename = request.POST["filename"]
	payload = {'token':token}
	response = requests.post("http://host21012.proxy.infralabs.cs.ui.ac.id/api/v1/validate",data=payload).json()
	
	if(response["status"] != "ok"):
		return "ERROR"
	now = time.time()
	limit = now+3600
	uri = "/downloads/"+filename
	ip = get_client_ip(request)
	# strings = "%d%s%s HIMITSU" % (limit,uri,ip) Kalau pakai IP selalu salah -> beda baca ip di nginx sama django
	strings = "%d%s HIMITSU" % (limit,uri)
	print(strings)
	stringb = bytes(strings, 'utf-8')
	c = hashlib.md5(stringb).digest()
	c = base64.b64encode(c).decode("utf-8").replace("=","").replace("+","-").replace("/","_")
	print(c)
	url = "http://host21012.proxy.infralabs.cs.ui.ac.id/downloads/%s?md5=%s&expires=%d" % (filename,c,limit)
	print(url)
	return JsonResponse({'status':'1',"url":url})

def get_client_ip(request):
    # x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    # if x_forwarded_for:
    #     ip = x_forwarded_for.split(',')[0]
    # else:
    ip = request.META.get('REMOTE_ADDR')
    return ip