from django.urls import path
from . import views

urlpatterns = [
    path('token',views.login, name="token"),
    path('register',views.register, name="register"),
]