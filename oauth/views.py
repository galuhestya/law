from django.http import HttpResponse, JsonResponse
from django.core.cache import cache
from dateutil import parser
import requests
import json
from django.views.decorators.csrf import csrf_exempt
from oauth.models import Oauth
import string
import random
import base64
import hashlib
import hmac

@csrf_exempt
def login(request):
	body = request.body

	body_unicode = body.decode('utf-8')
	body_data = json.loads(body_unicode)
	
	username = body_data["username"]
	password = body_data["password"]

	channel_secret = cache.get(username)

	if(channel_secret == None):
		try:
			channel_secret = Oauth.objects.get(username__iexact=username).secret
		except Oauth.DoesNotExist:
			return error_response(401, "Unauthorized")
	
	hashes = hmac.new(channel_secret.encode('utf-8'),
    	body, hashlib.sha256).digest()

	signature = base64.b64encode(hashes).decode("utf-8")

	user_signature = request.META.get("HTTP_X_SERVICE_SIGNATURE")

	if(signature == user_signature):
		token = generate_token()
		cache.set(token,username,timeout=300)

		response = {
			"status" : "ok",
			"token" : token,
		}
		return JsonResponse(response)
	else:
		return error_response(401,"Unauthorized. Unrecognized Signature")

@csrf_exempt
def register(request):
	body = request.body

	body_unicode = body.decode('utf-8')
	body_data = json.loads(body_unicode)
	
	username = body_data["username"]
	password = body_data["password"]
	display_name = body_data["displayName"]

	if(not cache.get(username) == None):
		return error_response(400, "Bad Request. Username not Available")

	s = generate_secret(32)

	u = Oauth(username=username,password=password,display_name=display_name,secret=s)
	u.save()

	cache.set(username,s)
	cache.persist(username)

	response = {
		"status" : "ok",
		"userId" : u.id,
		"displayName" : u.display_name,
		"secret" : s,
	}
	return JsonResponse(response)

def generate_token():
	token = ''.join(random.choice(string.ascii_uppercase+ string.ascii_lowercase + string.digits) for _ in range(36))
	return token

def generate_secret(seed):
	secret = ''.join(random.choice(string.ascii_uppercase+ string.ascii_lowercase + string.digits) for _ in range(seed))
	return secret

def error_response(code,description):
	return JsonResponse({
		"status" : "error",
		"description" : "%d %s" % (code, description), 
	})