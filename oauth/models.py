from django.db import models

# Create your models here.
class Oauth(models.Model):
	username = models.CharField(max_length=255,unique=True)
	password = models.CharField(max_length=255)
	display_name = models.CharField(max_length=255, blank=True)
	secret = models.CharField(max_length=255)